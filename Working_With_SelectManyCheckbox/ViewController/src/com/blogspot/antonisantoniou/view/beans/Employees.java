package com.blogspot.antonisantoniou.view.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.faces.model.SelectItem;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectManyCheckbox;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

public class Employees {

    private List<String> _departmentsSelected;

    public Employees() {
    }

    public void onSubmit(ActionEvent actionEvent) {
        BindingContext bctx = BindingContext.getCurrent();
        BindingContainer bindings = bctx.getCurrentBindingsEntry();
        DCIteratorBinding employeesIterator = (DCIteratorBinding)bindings.get("MyEmployeesViewIterator");
        Row employeeRow = employeesIterator.getCurrentRow();
        employeeRow.setAttribute("Departments", _departmentsSelected);
        OperationBinding commit = bindings.getOperationBinding("Commit");
        commit.execute();
    }

    public List<SelectItem> getDepartmentSelectItems() {
        List<String> departments = _getDepartments();
        List<SelectItem> departmentsSelectItems = new ArrayList<SelectItem>();
        for (String department : departments) {
            SelectItem item = new SelectItem(department, department, null);
            departmentsSelectItems.add(item);
        }
        return departmentsSelectItems;
    }

    public void setDepartmentsSelected(List<String> value) {
        _departmentsSelected = value;
    }

    public List<String> getDepartmentsSelected() {
        BindingContext bctx = BindingContext.getCurrent();
        BindingContainer bindings = bctx.getCurrentBindingsEntry();
        DCIteratorBinding employeesIterator = (DCIteratorBinding)bindings.get("MyEmployeesViewIterator");
        Row employeeRow = employeesIterator.getCurrentRow();
        String departmentsStr = (String)employeeRow.getAttribute("Departments");
        if (departmentsStr != null) {
            departmentsStr = departmentsStr.substring(1, departmentsStr.length() - 1);
            List<String> departments = Arrays.asList(departmentsStr.split(","));
            for (int x = 0; x < departments.size(); x++) {
                departments.set(x, departments.get(x).trim());
            }
            _departmentsSelected = departments;
        }
        return _departmentsSelected;
    }

    private List<String> _getDepartments() {
        List<String> departments = new ArrayList<String>();
        departments.add("IT");
        departments.add("Sales");
        departments.add("Marketing");
        return departments;
    }

}
