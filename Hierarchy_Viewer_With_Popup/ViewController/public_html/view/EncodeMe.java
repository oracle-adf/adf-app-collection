package view;

public class EncodeMe {
 
        private String initial="";
        private String scrubled="";
        private boolean single;
        private int size=0;
            
        
        public EncodeMe(String init) {
            initial=init;
            size=initial.length();
            single=initial.length()%2!=0;
        }
        
        public String scrumbler(String input){
            String C1;
            String C2;
            String output="";
            if(!single){
                C1=input.substring(0, size/2);
                C2=input.substring(size/2, size);
            
            }else{
                String Cm;
                C1=input.substring(0, (size/2));
                C2=input.substring(size/2+2, size);
                Cm=input.substring(size/2+1, size/2+1);
            }
            for(int i=0;i<size/2;i++){
//               output+= C1.substring(i,i+1) +C2.substring(C2.length()-i-1,C2.length()-i);
                output+= C1.substring(i,i+1) +C2.substring(i,i+1);
            }
            return output;
        }
        
        
        public static void main(String[] args) {
            EncodeMe foo = new EncodeMe("Process1234567dfghjk");
            String scrumbled=foo.scrumbler(foo.initial);
            System.out.println("SCRUBLED: "+scrumbled);
            
            for(int i=0;i<foo.size/2;i++){
                scrumbled=foo.scrumbler(scrumbled);
                System.out.println(i+" : "+scrumbled);
            }
        }
    }
