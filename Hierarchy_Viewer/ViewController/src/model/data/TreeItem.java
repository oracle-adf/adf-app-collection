
package model.data;

import java.util.List;
/** 
 * Object to hold the topology node details.
 */
public class TreeItem {
    private String text,action, imageName;


    private List<TreeItem> children;
    
    public TreeItem() {
        super();
    }
    /** 
     *  TreeItem method is used to initialize the properties.
     *  @params text, action, imageName
     */ 
    public TreeItem(String text,String action, String imageName){
      super();
      this.text = text;
      this.action = action;
      this.imageName = imageName;
    }


    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setChildren(List<TreeItem> children) {
        this.children = children;
    }

    public List<TreeItem> getChildren() {
        return children;
    }
}
