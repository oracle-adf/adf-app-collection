package model.data;

import java.util.ArrayList;

import java.util.List;

import org.apache.myfaces.trinidad.model.ChildPropertyTreeModel;
import org.apache.myfaces.trinidad.model.TreeModel;

import model.data.TreeItem;

public class HRTreeData {
    private Object instance = null;
    private transient TreeModel model = null;
    
    private ArrayList<TreeItem> root;
    
    public HRTreeData() {
        root = new ArrayList<TreeItem>();
        TreeItem node1 = new TreeItem("Level 1","Level One Description", "images/red.png");
        root.add(node1);
        
        ArrayList<TreeItem> node1Children = new ArrayList<TreeItem>();
        TreeItem node1Child1 = new TreeItem("Item 1","Item 1 in Level 2", "images/green.png");
        TreeItem node1Child2 = new TreeItem("Item 2","Item 2 in Level 2", "images/green.png");
        TreeItem node1Child3 = new TreeItem("Item 3","Item 3 in Level 2", "images/green.png");
        TreeItem node1Child4 = new TreeItem("Item 4","Item 4 in Level 2", "images/green.png");  
        TreeItem node1Child5 = new TreeItem("Item 5","Item 5 in Level 2", "images/green.png");
        TreeItem node1Child6 = new TreeItem("Item 6","Item 6 in Level 2", "images/green.png");  
        
        
            node1Children.add(node1Child1);
            node1Children.add(node1Child2);
            node1Children.add(node1Child3);
            node1Children.add(node1Child4);    
            node1Children.add(node1Child5);    
            node1Children.add(node1Child6);    
          node1.setChildren(node1Children);
        
        setListInstance(root);
    }
    
    /**
     * getModel method renders the child nodes required to populate the topology view.
     */    
    public TreeModel getModel() {
      if(model == null)
          model = new ChildPropertyTreeModel(instance,"children");
      return model;
    }
    
    /**
     * Setter method for properties defined in this class.
     */      
    public void setListInstance(List instance) {
      this.instance = instance;
      model = null;
    }    
}
